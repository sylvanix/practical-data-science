# practical data science

This repository is meant to serve two main purposes:  
	1. Catalog most things that a Data Scientist is expected to know.  
	2. Serve as a place to keep useful examples.    
	
These pages will sometimes reference sections in the sister repositories [practical machine learning](https://gitlab.com/sylvanix/practical-machine-learning) ,[practical data engineering](https://gitlab.com/sylvanix/practical-data-engineering) and [practical python](https://gitlab.com/sylvanix/practical-python).  
	
---	
	

### [Setting-up Your Work Environment](./data-science/python-env/python-env.md)  

	* Building Python on your system and creating a virtual environment
	* Building and running a Dockerized Python environment
	* Running containerized PostgreSQL and PGAdmin services
	
---	


### [Creating, Importing, and Exporting data](./data-science/import_export_data/data.md)    

	* Create a CSV with Pandas from a Python dictionary
	* Load the CSV to a PostgreSQL table
	* Export a PostgreSQL table to a CSV file
	* Save the results of a SQL query to a file
	* Generate fake datasets
	

---

### [SQL](./data-science/SQL/sql.md)   

	* Getting CSV data into a PostgreSQL table
	* Getting data from a PostgreSQL table to a CSV
	* Common queries, filtering, grouping
	* Table joins

---

### [Algorithms and Techniques](./data-science/algorithms/algorithms.md)  

```
* Dimensionality Reduction
* Assessing Feature Importance (Random Forests)
* Naive Bayes
```

---

### [Pre-requisite Statistics](./data-science/statistics/stats.md)  

```
* Bayes' Theorem 
* Linear Regression
*

```
  

## In Progress ...

