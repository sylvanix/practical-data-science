# Supervised Learning

In Supervised Learning labeled training data is used to build a model that allows us to make predictions about unseen or future data. The term "supervised" refers to the the labeled data.

## Classification

Classification is a subcategory of supervised learning where the goal is to predict the categorical class labels of new instances or data points based on past observations.

**Linear Classifiers**  



## Regression

Regression Analysis is the prediction of continuous outcomes. In regression analysis, we are given a number of predictor variables and a continuous response variable (outcome), and we try to find a relationship between those variables that allows us to predict an outcome.

