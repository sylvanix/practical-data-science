# Answers to Data Science Interview Questions  

* What are ways to estimate feature importance in a dataset?  


* W


# The non-technical questions  

- What is your greatest strength?  

- What is your greatest weakness?  

- Why should we hire you?  

- Why do you want to work here?  

- Why do you want to leave your current role?  

- Describe your most challenging project.  

- Where do you see yourself in 5 years?  

- Tell me about a time when you had to work with a difficult person.  

- Tell me about a time when you had to manage conflicting priorities.  

- Tell me about a time you surpassed people's expectations.  

- Tell me about a time when you had to learn something quickly.  

- Tell me about a time when you had to persuade someone.  

- Tell me about a time when you showed leadership.  

- Tell me about something you've accomplished that you're proud of.  

- What do you like to do outside of work?  

- Do you have any questions for me?  