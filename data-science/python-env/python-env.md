## Getting your Linux system ready for Data Science tasks

Most data scientists use Python with its large and mature "data ecosystem" tools. Some data scientists, myself included, choose to work in Linux, therefore, the instructions below assume that one has already installed their Linux-based operating system and is comfortable with a few basic command line operations.  

**Linux**  	

My Linux of choice, for both personal and professional use, is Debian. With Debian you can choose your level of “free” as well as your level of “newness”. I like to use the Debian non-free release. The latest (at the time of writing this) stable release can be downloaded here:  
https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/    
Besides the Stable release, you might consider Testing as your daily OS. With Testing you get a good balance between “bleeding edge” and “rock solid stability”. Don't let the name “Testing” fool you; any bugs are likely to be found and squashed in Debian Unstable, meaning that Testing is ready for daily use by developers. In fact, many debian-based Linux distros are Testing with a different windows manager and other cosmetic changes.  
Despite my preference for Debian, most any Linux distribution is great and worth using, so please find one that makes you happy.  

To enable deep-learning on your system, you'll need to install the Nvidia driver for your Nvidia GPU. To do that, add "contrib", "non-free" and "non-free-firmware" components to /etc/apt/sources.list, for example:  

```bash
deb http://deb.debian.org/debian/ bookworm main contrib non-free non-free-firmware
```


Now install the Nvidia driver:

```bash
sudo apt update
sudo apt install nvidia-driver firmware-misc-nonfree
```  


for more information, see here: https://wiki.debian.org/NvidiaGraphicsDrivers#Debian_12_.22Bookworm.22  

One last note in this section: I noticed, after installing Debian Bookworm, that the timezone was wrong. This lead me, after some looking, that it was not using a timeserver. The simple fix was to install Chrony:

```bash
sudo apt install chrony
```


**Python 3**  

Python 3 provides a native way to set up a virtual environment called 'venv'. Below we will build Python 3.11 on our system in the /opt directory. Then we will install a virtual environment in our home directory which links to the /opt version we installed. Then we will use pip to install most any package that we need into our virtual environment.  

If you are unsatisfied with this virtual environment for some reason, you can simply delete the directory and no trace will remain. You could even delete the version of Python that we will build and install in the /opt directory.  

#### Download and build Python 3.11 on your system
	
Install required packages:  

```bash
sudo apt install wget build-essential libreadline-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev libffi-dev zlib1g-dev liblzma-dev
```

Make a directory in /opt in which to install our new Python  

```bash
sudo mkdir -p /opt/python3.11.5/lib
```

Download Python 3.11.5 from:
https://www.python.org/downloads/
Uncompress and cd into the directory  

```bash
tar xvf Python-3.11.5.tar.xz
cd Python-3.11.5
```

Configure the install with our settings  

```bash
./configure --prefix=/opt/python3.11.5 --enable-shared --enable-optimizations
```


```bash
make -j16
```

Using --enable-optimizations will run tests which can take a while, so use more processor cores by using the -j flag. (I used the -j16 flag to use 16 of 24 cores). Check how many cores you have available by running ```nproc``` from the command line.  

Careful on this next step! Be sure to use ```altinstall```. Do NOT "make install"; it will overwrite your existing Python used by your operating system  

```bash
sudo make altinstall
```


Finished with this directory, cd back out and delete it

```bash
cd ..
sudo rm -rf Python3.11.5
```

**Adding some aliases to our .bashrc file**  

Add the block below to your .bashrc file. Replace '/home/username' in the block below with the path to your home directory. Note that I call my Python “py311”. I also have Python 3.10 installed, which I invoke with “py310”, to be clear which version I am using. If you will only use the one version, then you could simply call it “py3” for example.  

```bash
# PYTHON 3.11.5  
alias py311='/home/sylvanix/.py311/bin/python3.11' # to use this Python
# activate the py311 virtual environment
act_py311 () {
  . /home/sylvanix/.py311/bin/./activate
}
```


**Create a Python virtual environment**  

Next we will run our new Python from the command line with the -m flag to create the new virtual environment. I decided to call my virtual environment .py311, but call yours whatever you like. I preceded the name with a . to make it a hidden directory.

```bash
/opt/python3.11.5/bin/python3.11 -m venv .py311
```

Check for yourself that the new directory is there.

```bash
ls -l
```

Was the new .py311 directory listed?

Now let's use the first alias we created py311. Running this command starts our new Python 3.11.3 interpreter. This Python is used by our virtual environment. If you check in your .py311 directory you will find a symbolic link to /opt/python3.11.3/bin/python3.11

Run the py311 command and note the date and version number.

```bash
py311  
Python 3.11.5 (main, Sep 10 2023, 04:33:14) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> 
```

now do this in the interpreter:

```
>>> import this
```

If you see The Zen of Python, by Tim Peters, then it works! Type Ctrl-D to exit the interpreter and go back to the shell.

**Install some Python Data Science packages**  

Activate your new Python environment and install some packages.  

```bash
act_py311
(.py311)$ pip install torch scikit-learn pandas jupyter seaborn xgboost
```

Try out a Jupyter notebook:  
```bash
(.py311)$ python -m jupyter notebook
```
Aaack! Ugggh! Too much white on the screen.
Let's fix that.

```bash
(.py311)$ pip install jupyterthemes
```

Now select a dark theme, line *onedork*:  

```bash
(.py311)$ jt -t onedork
```

( leaving this reminder here for my future self - my favorite jupyterthemes configuration is:  
jt -t onedork -tf hack -tfs 12 -nf hack -hfs 13 -cellw 70% -lineh 150 )  

Your eyes will thank you next time you open a jupyter notebook.    

Sometimes I want to serve a notebook from my more powerful system (with a much newer GPU), over SSH so that I can access it from the browser on my laptop or less-powerful desktop. Here is how I do that:

SSH to the remote (more-powerful) system, let's say it has the local IP address of 192.168.1.123. Now launch Jupyter like this:  

```bash
jupyter notebook --ip="192.168.1.123" 
```

When the service runs, it will display a URL. Copy that and open it in your brower on your local machine.  


**Docker**  

Sometimes you may need/want to run your Python code from a container, so let's install and configure Docker on Debian 12 (Bookworm).

One could get the latest from the Debian repositories:

```sudo apt install docker.io```  

but, let's get teh latest version from Docker.

First, you must uninstall any previous installation of Docker

```bash
sudo apt purge docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

On a fresh install, no Docker will have been installed.

Now install some prerequisites:  

```bash
sudo apt update && sudo apt install ca-certificates curl gnupg
```

Create a directory to store the keyrings, unless it has already been created:  

```bash
sudo install -m 0755 -d /etc/apt/keyrings
```

Next, download the GPG key and store it in the keyrings dir  

```bash
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
```

Change the permissions of the docker.gpg file  

```bash
sudo chmod a+r /etc/apt/keyrings/docker.gpg
```

Set up the repository for Docker:  

```bash
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

Now we can install Docker: 

```bash
sudo apt update && sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

Test the install:  

```bash
sudo docker run hello-world
```


Remember to add yourself to the docker group:  

```bash
sudo usermod -aG docker $USER
```

Log out and them back in for the change to take effect.

Now try it without sudo:  

```bash
docker run hello-world
```

#### Building and running a Dockerized Python environment  

Instead of building Python on your system, you could, after installing Docker and ensuring that you are in the docker group, run a Python Linux-based container with libraries already installed. I have an example here: https://gitlab.com/sylvanix/linux-dev-docker-image/-/tree/main/py-bullseye  
This is a more advanced option since you'll need to be familiar with Docker. If you choose this route, you'll want to edit the Dockerfile to use your own user-id and group-id on your host system. Also you'll need to edit the build_run.sh file so that the second volume bind-mount (the lines beginning with -v) reflects a path on your own host system. These volumes are how files in a container can be read from and written to the host system.  



#### Running containerized PostgreSQL and PGAdmin services  

Since so much of a Data Scientist's work involves Databases it's very useful to install, either on you system, or in a container, your own database for practice and for code development. When it comes to relational databases, I prefer PostgreSQL. A popular UI for Postgres is PGAdmin; it makes viewing and querying tables easy.

You can install PostgreSQL and PGAdmin4 from your Linux distribution's package manager. Alternatively, you can use Docker-compose to run then in containers. Here is a link to my **Practical Data Engineering** repository where I show how to do that: https://gitlab.com/sylvanix/practical-data-engineering/-/blob/main/postgres_pgadmin_sqlalchemy/Readme.md

