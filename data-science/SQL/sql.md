# SQL  

**Getting CSV data into a PostgreSQL table**  

Option 1: create from scratch using SQL

```sql
CREATE table members (
	index bigint,
	name varchar(25),
	level int,
	location varchar(25),
	credits real
);
```

```sql
INSERT INTO members (index, name, level, location, credits)
	VALUES (0, 'Bif', 4, 'Portland', 101.2),
	(1, 'Telemaque', 9, 'Huntsville', 93.3),
	(2, 'Mikey', 3, 'Glasgow', 23.4),
	(3, 'Kyra', 6, 'Kyiv', 226.8),
	(4, 'Miziki', 8, 'Funabashi', 338.2);
```


Option 2: import from a CSV using the SQL COPY FROM syntax

```sql
COPY table_name
FROM '/home/my_home/my_file.csv'
WITH (FORMAT CSV, HEADER);
```

Option 3: import from a CSV using Pandas
An example of this method is in the previous section:
[import\_export\_data](https://gitlab.com/sylvanix/practical-data-science/data-science/import\_export\_data/data.md)  




**Getting CSV data out from a PostgreSQL table**  

Option 1: SQL's COPY TO syntax

```sql
COPY members
TO '/home/my_home/members.txt'
WITH (FORMAT CSV, HEADER, DELIMITER ',');
```

Option 2: export to a CSV using Pandas
An example of this method is in the previous section:
[import\_export\_data](https://gitlab.com/sylvanix/practical-data-science/data-science/import\_export\_data/data.md)  

---

**Common Queries**  

For the queries that follow, we'll use the database table "members" shown below:  
![Image](./members_table.png)  

return all columns from every row

```sql
SELECT * FROM members
```

return only certain columns from every row

```sql
SELECT name, location FROM members
```

filter using WHERE

```sql
SELECT * FROM members WHERE level >= 6
```


**Aggregation using the GROUP BY clause**

```sql
SELECT level, COUNT(*)
FROM members
GROUP BY level
ORDER BY level DESC
```

The result shows the unique levels and the number of rows having that value  
Besides COUNT(), we can use the GROUP BY aggregation functions AVG(), MIN(), MAX(), & SUM()

 


