import numpy as np

np.random.seed(97)

def relu(x):
    return (x > 0) * x

def relu2deriv(output):
    return output>0

X = np.array( [[ 1, 0, 1 ],
               [ 0, 1, 1 ],
               [ 0, 0, 1 ],
               [ 1, 1, 1 ] ] )

y = np.array([ [1],
               [1],
               [0],
               [0] ])
    
alpha = 0.01
hidden_size = 5

weights_0_1 = 2*np.random.random((3,hidden_size)) - 1
weights_1_2 = 2*np.random.random((hidden_size,1)) - 1

epochs = 1000
for iteration in range(epochs):
    error = 0
    
    # iterate over the training examples
    for i in range(X.shape[0]):
        layer_0 = X[i:i+1]

        # forward pass
        layer_1 = relu(np.dot(layer_0, weights_0_1))
        layer_2 = np.dot(layer_1, weights_1_2)

        error += np.sum((layer_2 - y[i:i+1]) ** 2)

        # backpropagation of error
        layer_2_delta = 2*(layer_2 - y[i:i+1])
        layer_1_error = np.dot(layer_2_delta, weights_1_2.T) 
        layer_1_delta = layer_1_error * relu2deriv(layer_1)

        weights_1_2 -= alpha * np.dot(layer_1.T, layer_2_delta)
        weights_0_1 -= alpha * np.dot(layer_0.T, layer_1_delta)
        if iteration==epochs-1: print(layer_2[0])

    if(iteration % 100 == 0):
        print("Error: " + str(error))
