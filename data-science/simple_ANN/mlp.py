'''
An implementation, with example usage, of a multi-layer perceptron (mlp).
'''

import numpy as np


class Neural_Network(object):
    def __init__(self, lr=0.01):
        #parameters
        self.inputSize = 3
        self.hiddenSize = 5
        self.outputSize = 1
        self.lr = lr

        # WEIGHTS
        # (inputSize x hiddenSize) weight matrix from input to hidden layer
        self.W1 = np.random.randn(self.inputSize, self.hiddenSize) 
        # (hiddenSize x outputSize) weight matrix from hidden to output layer
        self.W2 = np.random.randn(self.hiddenSize, self.outputSize)

    def forward(self, X):
        '''forward propagation through the network'''
        # dot product of X (input) and first set weights
        self.z = np.dot(X, self.W1)
        # activation function
        self.z2 = self.sigmoid(self.z)
        # dot product of hidden layer (z2) and second set of weights
        self.z3 = np.dot(self.z2, self.W2)
        # final activation function
        o = self.sigmoid(self.z3)
        return o 

    def sigmoid(self, s):
        '''activation function'''
        return 1/(1+np.exp(-s))

    def sigmoidPrime(self, s):
        '''derivative of sigmoid'''
        return s * (1 - s)

    def backward(self, X, y, o):
        '''backward propgate through the network'''
        self.o_error = y - o # error in output
        # applying derivative of sigmoid to error
        self.o_delta = self.o_error*self.sigmoidPrime(o)
        # z2 error: how much our hidden layer weights contributed to output error
        self.z2_error = self.o_delta.dot(self.W2.T)
        # applying derivative of sigmoid to z2 error
        self.z2_delta = self.z2_error*self.sigmoidPrime(self.z2)

        # adjusting first set (input --> hidden) weights
        self.W1 += self.lr * X.T.dot(self.z2_delta)
        # adjusting second set (hidden --> output) weights
        self.W2 += self.lr * self.z2.T.dot(self.o_delta)

    def train (self, X, y):
        '''train the network with the labeled dataset'''
        o = self.forward(X)
        self.backward(X, y, o)


########################################################################

# The dataset
X = np.array(([1,1,1],[1,0,0],[0,1,0],[0,0,1],[1,1,0], [0,1,1]), dtype=float)
y = np.array(([0], [1], [1], [0], [0], [1]), dtype=float)

# Train
NN = Neural_Network(lr=0.01)
for i in range(100000):
    if i % 10000 == 0:
        # mean squared loss (mse)
        print( "Loss: " + str(np.mean(np.square(y - NN.forward(X)))) )
    NN.train(X, y)

# Predict
print( "\nPredicted output:" )
for x in NN.forward(X):
    print('    ', x) 
