### Bayes' Theorem  

Bayes's Theorem describes the probability of an event, based on prior knowledge of conditions that might be related to the event. It is the basis of the probabilistic classification algorithm **Naïve Bayes**, which assigns a class to an element of a set that is most probable according to Bayes' theorem. The algorithm is called _Naïve_ because we assume that the predictive features are mutually independent. We won't discuss the classification algorithm here, but will learn how to use Bayes' Theorem to assign probabilities to events given prior information.  

Let A and B denote two events. In Bayes' theorem, P(A|B) is the probability of A given that B is true. It is computed as:

```
P(A|B) = ( P(B|A) P(A) ) / P(B)
```

P(A|B) is a conditional probability: the probability of event A occurring given that B is true. It is also called the posterior probability of A given B.  

P(B|A) is also a conditional probability: the probability of event B occurring given that A is true. It can also be interpreted as the likelihood of A given a fixed B because P (B∣A) = L (A∣B).  (L is the likelihood)  

P(A) and P (B) are the probabilities of observing A and B respectively without any given conditions; they are known as the prior probability and marginal probability.  

Let's consider some examples.  

What is the probability that there is an uncontrolled fire, given that smoke has been sighted if the following is true (example adapted from https://www.mathsisfun.com/data/bayes-theorem.html):  

* uncontrolled fires occur 1% of the time.
* smoke can be seen 10% of the time (cookouts).  
* 90% of the uncontrolled fires make visible smoke.  

```
P(fire|smoke) = P(smoke|fire) P(fire) / p(smoke)  
	= 90% x 1% / 10% = (0.9 * 0.01)/0.1 = 0.09 = 9%
```

Ok, that was too contrived. Here's another:

Given the following information, what is the probability that a woman has breast cancer if she has a positive mammogram result?  

* 1% of women over 50 have breast cancer.
* 90% of women who test positive actually have breast cancer.
* 8% of women will have false positives.

In this case we will use an alternate form of Bayes' Theorem, recognizing that we can re-write P(B) as P(B|A)P(A) + P(B|~A)P(~A), where ~ means "not".  

```
P(A|B) = ( P(B|A) P(A) ) / ( P(B|A) P(A) + P(B|~A) P(~A) )
```

Let P(C) be the probability that a woman over 50 has breast cancer and P(C|pos) be the probability that she has the disease given a positive test result.  
P(C) = 0.01
P(~C) = 1 - 0.01 = 0.99
P(pos|C) = 0.9
P(pos|~C) = 0.08

```
P(C|pos) = ( P(pos|C) * P(C) ) / ( P(pos|C) * P(C) + P(pos|~C) * P(~C) )
 = (0.9 * 0.01) / ((0.9 * 0.01) + (0.08 * 0.99)) = 0.1
```

