import random
import pandas as pd
from mimesis.locales import Locale
from mimesis.schema import Fieldset


institutions = [
    "Alphabet Inc.", "Pfizer", "Lufthansa", "Samsung",
    "Nintendo", "Playmobil", "Adidas", "Yamaha"
]

fs = Fieldset(locale=Locale.EN, i=5)
df_en = pd.DataFrame.from_dict(
    {
        "ID": fs("increment"),
        "Name": fs("person.full_name"),
        "Email": fs("email"),
        "Address": fs("address")
    }
)

fs = Fieldset(locale=Locale.JA, i=2)
df_ja = pd.DataFrame.from_dict(
    {
        "ID": fs("increment"),
        "Name": fs("person.full_name"),
        "Email": fs("email"),
        "Address": fs("address")
    }
)

fs = Fieldset(locale=Locale.DE, i=1)
df_de = pd.DataFrame.from_dict(
    {
        "ID": fs("increment"),
        "Name": fs("person.full_name"),
        "Email": fs("email"),
        "Address": fs("address")
    }
)

# combine the dataframes from the different locales
combined = [df_en, df_ja, df_de]
df = pd.concat(combined, axis=0, ignore_index=True)

# remeve the ID column
del df["ID"]

# shuffle the rows
df = df.sample(frac=1).reset_index(drop=True)

# add a new column for "Institution"
df['Institution'] = [
    institutions[random.randint(0,len(institutions)-1)]
    for _ in range(df.shape[0])
]

# save to a csv file
df.to_csv("contacts.csv", encoding='utf-8', index=False)
