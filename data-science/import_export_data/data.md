## Importing and Exporting Data  


**Create a CSV using Pandas**  

```python
import pandas as pd

# create a data frame from a dict
df = pd.DataFrame.from_dict({
    "Name": ["Bif", "Telemaque", "Mikey", "Kyra"],
    "Level": [4, 9, 3, 6],
    "Location": ["Portland", "Huntsville", "Glasgow", "Kyiv"],
    "Credits": [101.2, 93.3, 23.4, 226.8]
})

# Add a new column
keys = df.columns.values
vals = ["Mikko", 8, "Funabashi", 338.2]
new_row = dict(zip(keys,vals))
df = pd.concat([df, pd.DataFrame([new_row])], ignore_index=True)

# save to a csv file
df.to_csv("members.csv", encoding='utf-8', index=False)
```



**Load the CSV data to PostgreSQL**  

```python
import pandas as pd
from sqlalchemy import create_engine, inspect

df = pd.read_csv('/home/sylvanix/data/members.csv')
df.columns = [c.lower() for c in df.columns] # don't use upper-case or spaces

engine = create_engine('postgresql://sylvanix:passwd@localhost:5432/practice')

df.to_sql("members", engine)
```

Check that the table was written:

![Image](./members_table.png)  


**Export the Postgres table to a CSV file**  

```python
from sqlalchemy import create_engine, inspect
import pandas as pd

connection = create_engine(
    'postgresql://sylvanix:passwd@localhost:5432/practice'
).connect()

df = pd.read_sql_table('members', connection)
df.to_csv("members_from_postgres.csv", encoding="utf-8", index=True)
```


**Save a SQL query to CSV** 
 
```python
from sqlalchemy import create_engine
import pandas as pd

engine = create_engine('postgresql://sylvanix:passwd@localhost:5432/practice')

sql = "SELECT * FROM members WHERE level > 4"
df = pd.read_sql(sql,con=engine)
```


**Generate fake datasets using Mimesis**  

```python
import random
import pandas as pd
from mimesis.locales import Locale
from mimesis.schema import Fieldset


institutions = [
    "Alphabet Inc.", "Pfizer", "Lufthansa", "Samsung",
    "Nintendo", "Playmobil", "Adidas", "Yamaha"
]

fs = Fieldset(locale=Locale.EN, i=5)
df_en = pd.DataFrame.from_dict(
    {
        "ID": fs("increment"),
        "Name": fs("person.full_name"),
        "Email": fs("email"),
        "Address": fs("address")
    }
)

fs = Fieldset(locale=Locale.JA, i=2)
df_ja = pd.DataFrame.from_dict(
    {
        "ID": fs("increment"),
        "Name": fs("person.full_name"),
        "Email": fs("email"),
        "Address": fs("address")
    }
)

fs = Fieldset(locale=Locale.DE, i=1)
df_de = pd.DataFrame.from_dict(
    {
        "ID": fs("increment"),
        "Name": fs("person.full_name"),
        "Email": fs("email"),
        "Address": fs("address")
    }
)

# combine the dataframes from the different locales
combined = [df_en, df_ja, df_de]
df = pd.concat(combined, axis=0, ignore_index=True)

# remeve the ID column
del df["ID"]

# shuffle the rows
df = df.sample(frac=1).reset_index(drop=True)

# add a new column for "Institution"
df['Institution'] = [
    institutions[random.randint(0,len(institutions)-1)]
    for _ in range(df.shape[0])
]

# save to a csv file
df.to_csv("contacts.csv", encoding='utf-8', index=False)
```


