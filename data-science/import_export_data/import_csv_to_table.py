import pandas as pd
from sqlalchemy import create_engine, inspect

df = pd.read_csv('/home/sylvanix/data/members.csv')
df.columns = [c.lower() for c in df.columns] # don't use upper-case or spaces

engine = create_engine('postgresql://sylvanix:passwd@localhost:5432/practice')

df.to_sql("members", engine)
