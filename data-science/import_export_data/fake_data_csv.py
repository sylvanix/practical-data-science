import pandas as pd

# create a data frame from a dict
df = pd.DataFrame.from_dict({
    "Name": ["Bif", "Telemaque", "Mikey", "Kyra", "Ji-yoo"],
    "Level": [4, 9, 3, 6, 6],
    "Location": ["Portland", "Huntsville", "Glasgow", "Kyiv", "Sejong"],
    "Credits": [101.2, 93.3, 23.4, 226.8, 256.7]
})

# Add a new column
keys = df.columns.values
vals = ["Mizuki", 8, "Funabashi", 338.2]
new_row = dict(zip(keys,vals))
df = pd.concat([df, pd.DataFrame([new_row])], ignore_index=True)

# save to a csv file
df.to_csv("members.csv", encoding='utf-8', index=False)
