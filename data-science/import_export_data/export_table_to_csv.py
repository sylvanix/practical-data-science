from sqlalchemy import create_engine, inspect
import pandas as pd

connection = create_engine(
    'postgresql://sylvanix:passwd@localhost:5432/practice'
).connect()

df = pd.read_sql_table('members', connection)
df.to_csv("members_from_postgres.csv", encoding="utf-8", index=True)
